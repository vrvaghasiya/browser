package com.creativeinfoway.kiosk.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import androidx.annotation.RequiresApi;

public class TimeUrl {

    public String url;
    public String start_time, end_time;
    public int startUniqueID,endUniqueID;

    public TimeUrl(String url, String starttime, String endtime,int start_unique_id,int end_unique_id) {
        this.url = url;
        this.start_time = starttime;
        this.end_time = endtime;
        this.startUniqueID=start_unique_id;
        this.endUniqueID=end_unique_id;
    }

    public static ArrayList<TimeUrl> getSavedURLFromPreference(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Mpref", 0);
        if (sharedPreferences.contains(
                "savelist"
        )) {
            final Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<TimeUrl>>() {
            }.getType();
            return gson.fromJson(sharedPreferences.getString("savelist", ""), type);
        }
        return new ArrayList<TimeUrl>();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void deleteFromSharedPreference(Context context, int  startUniqueID) {

        SharedPreferences sharedPreferences = context.getSharedPreferences("Mpref", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        final Gson gson = new Gson();

        ArrayList<TimeUrl> arrayList = TimeUrl.getSavedURLFromPreference(context);
        List<TimeUrl> timeUrlList = arrayList.stream().filter(s -> s.startUniqueID==startUniqueID).collect(Collectors.toList());
        for (TimeUrl timeUrl1 : timeUrlList) {
            arrayList.remove(timeUrl1);
        }

        String serializedObject = gson.toJson(arrayList);
        sharedPreferencesEditor.putString("savelist", serializedObject);
        sharedPreferencesEditor.apply();
    }

    public static String getCurrentUrl(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString("currenturl", "");
    }

    public static void setCurrentUrl(final String url, final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString("currenturl", url).apply();
    }

    public static String getDefaultUrl(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString("url", "");
    }

    public static void setDefaultUrl(final String url, final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putString("url", url).apply();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void saveURLToSharedPreference(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Mpref", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        final Gson gson = new Gson();

        ArrayList<TimeUrl> arrayList = TimeUrl.getSavedURLFromPreference(context);
        arrayList.add(TimeUrl.this);

        String serializedObject = gson.toJson(arrayList);
        sharedPreferencesEditor.putString("savelist", serializedObject);
        sharedPreferencesEditor.apply();
    }

    @androidx.annotation.RequiresApi(api = Build.VERSION_CODES.N)
    public void editURLFromSharedPreference(Context context, String updated_Url, String update_StartTime, String update_EndTime) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Mpref", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        final Gson gson = new Gson();

        ArrayList<TimeUrl> arrayList = TimeUrl.getSavedURLFromPreference(context);
        List<TimeUrl> timeUrla = arrayList.stream().filter(s -> s.url.equals(this.url)).collect(Collectors.toList());
        for (TimeUrl timeUrl1 : timeUrla) {
            timeUrl1.url = updated_Url;
            timeUrl1.start_time = update_StartTime;
            timeUrl1.end_time = update_EndTime;
        }

        String serializedObject = gson.toJson(arrayList);
        sharedPreferencesEditor.putString("savelist", serializedObject);
        sharedPreferencesEditor.apply();
    }
}
