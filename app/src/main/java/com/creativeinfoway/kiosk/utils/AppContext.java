package com.creativeinfoway.kiosk.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;

import com.creativeinfoway.kiosk.activity.MainActivity;
import com.creativeinfoway.kiosk.receiver.OnScreenOffReceiver;

import androidx.annotation.RequiresApi;

public class AppContext extends Application {

    private AppContext instance;
    private PowerManager.WakeLock wakeLock;
    private OnScreenOffReceiver onScreenOffReceiver;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationLifecycleHandler handler = new ApplicationLifecycleHandler();
        registerActivityLifecycleCallbacks(handler);
        registerComponentCallbacks(handler);
        instance = this;
        registerKioskModeScreenOffReceiver();
    }
    private void registerKioskModeScreenOffReceiver() {
        // register screen off receiver
        final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        onScreenOffReceiver = new OnScreenOffReceiver();
        registerReceiver(onScreenOffReceiver, filter);
    }

    @SuppressLint("InvalidWakeLockTag")
    public PowerManager.WakeLock getWakeLock() {
        if (wakeLock == null) {
            // lazy loading: first call, create wakeLock via PowerManager.
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "wakeup");
        }
        return wakeLock;
    }

    public class ApplicationLifecycleHandler implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {
        private final String TAG = ApplicationLifecycleHandler.class.getSimpleName();
        private boolean isInBackground = false;
        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {
        }
        @Override
        public void onActivityStarted(Activity activity) {
            Log.d(TAG, "app went to started");
        }
        @Override
        public void onActivityResumed(Activity activity) {
            if (isInBackground) {
                Log.d(TAG, "app went to foreground");
                isInBackground = false;
            }
        }
        @Override
        public void onActivityPaused(Activity activity) {
            Log.d(TAG, "app went to paused");
        }
        @Override
        public void onActivityStopped(Activity activity) {
            Log.d(TAG, "app went to stopped");
        }
        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }
        @Override
        public void onActivityDestroyed(Activity activity) {
            Log.d(TAG, "app went to destroyed");
        }
        @Override
        public void onConfigurationChanged(Configuration configuration) {
        }

        @Override
        public void onLowMemory() {
        }
        @Override
        public void onTrimMemory(int i) {
            if (i == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN) {
                Log.d(TAG, "app went to background");
                isInBackground = true;
                new Handler().postDelayed(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.Q)
                    @Override
                    public void run() {
                        restoreApp();
                    }
                }, 1000);
            }
        }


        private void restoreApp() {
            // Restart activity
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(i);
        }
    }
}
