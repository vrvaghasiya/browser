package com.creativeinfoway.kiosk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.creativeinfoway.kiosk.R;
import com.creativeinfoway.kiosk.utils.TimeUrl;

import java.util.ArrayList;

public class TimeURLAdapter extends BaseAdapter {
    public TimeURLListener timeURLListener;
    Context context;
    ArrayList<TimeUrl> timeUrlArrayList = new ArrayList<>();
    private DataViewHolder dataViewHolder;

    public TimeURLAdapter(Context context, ArrayList<TimeUrl> timeUrlArrayList_P, TimeURLListener timeURLListener_) {
        this.context = context;
        this.timeUrlArrayList = timeUrlArrayList_P;
        this.timeURLListener = timeURLListener_;
    }

    @Override
    public int getCount() {
        return timeUrlArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if (view == null) {
            dataViewHolder = new DataViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.custom_url_list, null);
            dataViewHolder.tvUrl = view.findViewById(R.id.tv_url);
            dataViewHolder.tvTime = view.findViewById(R.id.tv_time);
            dataViewHolder.ivEdit = view.findViewById(R.id.iv_edit);
            dataViewHolder.ivDelete = view.findViewById(R.id.iv_delete);
            view.setTag(dataViewHolder);
        } else {
            dataViewHolder = (DataViewHolder) view.getTag();
        }

        dataViewHolder.tvUrl.setText(timeUrlArrayList.get(i).url);
        dataViewHolder.tvTime.setText(timeUrlArrayList.get(i).start_time+" - "+timeUrlArrayList.get(i).end_time);
        dataViewHolder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeURLListener.onEditClick(timeUrlArrayList.get(i));

            }
        });
        dataViewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timeURLListener.onDeleteClick(timeUrlArrayList.get(i));
            }
        });

        return view;
    }

    public interface TimeURLListener {
        void onDeleteClick(TimeUrl timeUrl);

        void onEditClick(TimeUrl timeUrl);
    }

    public class DataViewHolder {
        public TextView tvUrl;
        public TextView tvTime;
        public ImageView ivDelete;
        public ImageView ivEdit;

        DataViewHolder() {

        }
    }
}
