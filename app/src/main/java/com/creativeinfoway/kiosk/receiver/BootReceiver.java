package com.creativeinfoway.kiosk.receiver;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;

import com.creativeinfoway.kiosk.activity.MainActivity;
import com.creativeinfoway.kiosk.activity.SettingsActivity;
import com.creativeinfoway.kiosk.utils.AppContext;
import com.creativeinfoway.kiosk.utils.TimeUrl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import androidx.annotation.RequiresApi;

import static com.creativeinfoway.kiosk.activity.MainActivity.webView;
import static com.creativeinfoway.kiosk.activity.SettingsActivity.END_TIME;
import static com.creativeinfoway.kiosk.activity.SettingsActivity.START_TIME;


public class BootReceiver extends BroadcastReceiver {

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d("====Current Time", String.valueOf(getCurrentTIme()));
        if(action.equals(Intent.ACTION_BOOT_COMPLETED)){

            AppContext ctx = (AppContext) context.getApplicationContext();
            wakeUpDevice(ctx);
            Intent intentsend=new Intent(context,MainActivity.class);
            intentsend.putExtra("BOOT","BOOT");
            intentsend.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context .startActivity(intentsend);


        }
        else if (action.equals(START_TIME)) {
            ArrayList<TimeUrl> arrayList = TimeUrl.getSavedURLFromPreference(context);
            List<TimeUrl> timeUrlList = arrayList.stream().filter(s -> s.start_time.equals(getCurrentTIme())).collect(Collectors.toList());
            for(TimeUrl timeUrl:timeUrlList)
            {
                TimeUrl.setCurrentUrl(timeUrl.url, context);
                if(webView!=null) {
                    webView.loadUrl(timeUrl.url);
                }
            }
        } else if (action.equals(END_TIME)) {
            String url=TimeUrl.getDefaultUrl(context);
            TimeUrl.setCurrentUrl(url, context);
            if(webView!=null) {
                webView.loadUrl(url);
            }
//            Intent myIntent = new Intent(context, MainActivity.class);
//            myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(myIntent);

        }
    }

    private String getCurrentTIme() {
        Calendar calendar = Calendar.getInstance();
        String currentTime = parseDateToddMMyyyy(calendar.getTime().toString());
        return currentTime;
    }

    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "EEE MMM dd HH:mm:ss Z yyyy";
        String outputPattern = "h:mm:ss a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
    private void wakeUpDevice(AppContext context) {
        PowerManager.WakeLock wakeLock = context.getWakeLock(); // get WakeLock reference via AppContext
        if (wakeLock.isHeld()) {
            wakeLock.release(); // release old wake lock
        }
        // create a new wake lock...
        wakeLock.acquire();

        // ... and release again
        wakeLock.release();
    }
}
