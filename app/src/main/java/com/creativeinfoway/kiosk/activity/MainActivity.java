package com.creativeinfoway.kiosk.activity;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.creativeinfoway.kiosk.R;
import com.creativeinfoway.kiosk.receiver.BootReceiver;
import com.creativeinfoway.kiosk.utils.OnSwipeListener;
import com.creativeinfoway.kiosk.utils.PrefUtils;
import com.creativeinfoway.kiosk.utils.TimeUrl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;

import static com.creativeinfoway.kiosk.activity.SettingsActivity.END_TIME;
import static com.creativeinfoway.kiosk.activity.SettingsActivity.START_TIME;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    public static String URL1 = "https://gpaymenu.foodzaps.com/menu/?id=winson2&start=1&displayColumn=3&rows=5,4,5,6,4,6,7,4,7&pageNo=1";
    public static String URL2 = "https://gpaymenu.foodzaps.com/menu/?id=winson2&start=1&displayColumn=3&rows=5,4,5,6,4,6,7,4,7&pageNo=2";
    public static String URL3 = "https://gpaymenu.foodzaps.com/menu/?id=winson2&start=1&displayColumn=3&rows=5,4,5,6,4,6,7,4,7&pageNo=3";
    public static String URL4 = "https://www.google.com/";
    public static String BOOT = "boot";
    private final List blockedKeys = new ArrayList(Arrays.asList(KeyEvent.KEYCODE_VOLUME_DOWN, KeyEvent.KEYCODE_VOLUME_UP));
    OnSwipeListener onSwipeListener;
    GestureDetectorCompat detectorCompat;
    AlarmManager alarmStart, alarmEnd;
    Intent intentStart, intentEnd;
    PendingIntent pendingStart, pendingEnd;
    public static WebView webView;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        setContentView(R.layout.activity_main);
        initID();
        initSwipe();
        webViewConfig();
        initURL();
        setBackgroundImage();
        zoomInZoomOut();
        cacheSetting();
        if (getIntent().getStringExtra("BOOT") != null) {
            getIntent().removeExtra("BOOT");
            initBootReceiverData();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void initBootReceiverData() {
        alarmStart = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        intentStart = new Intent(getApplicationContext(), BootReceiver.class);
        alarmEnd = (AlarmManager) getSystemService(ALARM_SERVICE);
        intentEnd = new Intent(getApplicationContext(), BootReceiver.class);
        ArrayList<TimeUrl> timeUrlArrayList = TimeUrl.getSavedURLFromPreference(getApplicationContext());
        for (int i = 0; i < timeUrlArrayList.size(); i++) {
            ArrayList<PendingIntent> pendingIntentArrayListStart = new ArrayList<>();
            intentStart.setAction(START_TIME);
            pendingStart = PendingIntent.getBroadcast(getApplicationContext(), timeUrlArrayList.get(i).startUniqueID, intentStart, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmStart.setExact(AlarmManager.RTC_WAKEUP, SettingsActivity.convertTimeToMillisecond(timeUrlArrayList.get(i).start_time), pendingStart);
            pendingIntentArrayListStart.add(pendingStart);

            ArrayList<PendingIntent> pendingIntentArrayListEnd = new ArrayList<>();
            intentEnd.setAction(END_TIME);
            pendingEnd = PendingIntent.getBroadcast(getApplicationContext(), timeUrlArrayList.get(i).endUniqueID, intentEnd, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmEnd.setExact(AlarmManager.RTC_WAKEUP, SettingsActivity.convertTimeToMillisecond(timeUrlArrayList.get(i).end_time), pendingEnd);
            pendingIntentArrayListEnd.add(pendingEnd);

            Log.d("=====url", timeUrlArrayList.get(i).url);
            Log.d("=====startTime", timeUrlArrayList.get(i).start_time);
            Log.d("=====endTime", timeUrlArrayList.get(i).end_time);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initSwipe() {
        onSwipeListener = new OnSwipeListener() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public boolean onSwipe(Direction direction) {
                if (direction == Direction.right) {
                    Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                    startActivity(intent);
                    return true;
                }
                return super.onSwipe(direction);
            }
        };
        detectorCompat = new GestureDetectorCompat(getApplicationContext(), onSwipeListener);
        webView.setOnTouchListener(this);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return detectorCompat.onTouchEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        detectorCompat.onTouchEvent(ev);
        return super.dispatchTouchEvent(ev);
    }

    public void loadURL(String url) {
        webView.loadUrl(url);
    }


    private void initID() {
        webView = findViewById(R.id.webview);
        PrefUtils.setKioskModeActive(true, getApplicationContext());
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    public void initURL() {
        if (TimeUrl.getDefaultUrl(getApplicationContext()).equals("")) {
            showPopUp();
        } else {
            loadURL(TimeUrl.getCurrentUrl(getApplicationContext()));
        }
    }


    private void showPopUp() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("URL Configuration");
        dialogBuilder.setMessage("Please Enter Default URL");
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton("SETTING", new DialogInterface.OnClickListener() {
            @androidx.annotation.RequiresApi(api = Build.VERSION_CODES.O)
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });

        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onStart() {
        super.onStart();

    }

    @SuppressLint("SetJavaScriptEnabled")
    private void webViewConfig() {
        webView.setWebViewClient(new WebViewClient());
        WebSettings webSettings = webView.getSettings();
        webSettings.setDomStorageEnabled(true);
        webSettings.setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
    }

    private void setBackgroundImage() {
        webView.setBackgroundColor(0);
        webView.setBackgroundColor(Color.TRANSPARENT);
        webView.setBackgroundResource(R.drawable.bg);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
    }

    private void zoomInZoomOut() {
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
    }

    private void cacheSetting() {
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getPath());
        webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        }
        // prevent Backpress press button.
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (!hasFocus) {
            // It will surely prevent long press button.
            // Close every kind of system dialog
            Intent closeDialog = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
            sendBroadcast(closeDialog);
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        ///Disable the volume button
        if (blockedKeys.contains(event.getKeyCode())) {
            return true;
        } else {
            return super.dispatchKeyEvent(event);
        }
    }

}
