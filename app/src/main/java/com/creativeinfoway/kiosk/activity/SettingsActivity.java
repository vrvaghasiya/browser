package com.creativeinfoway.kiosk.activity;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.creativeinfoway.kiosk.R;
import com.creativeinfoway.kiosk.adapter.TimeURLAdapter;
import com.creativeinfoway.kiosk.receiver.BootReceiver;
import com.creativeinfoway.kiosk.utils.TimeUrl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    public static String START_TIME = "START_TIME";
    public static String END_TIME = "END_TIME";
    EditText etStartTime, etEndTime, etUrl, etDefaultUrl;
    Button btnSave, btnReboot, btnAdd;
    ListView listView;
    TimeUrl timeUrl;
    TimeURLAdapter timeURLAdapter;
    AlarmManager alarmStart, alarmEnd;
    Intent intentStart, intentEnd;
    PendingIntent pendingStart, pendingEnd;
    int mStartUniqueId,mEndUniqueID;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initIDs();
        initData();
        loadListView();
        initAlarmConfig();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void initIDs() {
        etStartTime = findViewById(R.id.et_start_time);
        etEndTime = findViewById(R.id.et_end_time);
        etUrl = findViewById(R.id.et_url);
        btnSave = findViewById(R.id.buttonSave);
        btnReboot = findViewById(R.id.btn_reboot_device);
        btnAdd = findViewById(R.id.buttonadd);
        listView = findViewById(R.id.listview);
        etDefaultUrl = findViewById(R.id.et_default_url);
        etStartTime.setOnClickListener(this);
        etEndTime.setOnClickListener(this);
    }

    private void initData() {
        etDefaultUrl.setText(TimeUrl.getDefaultUrl(getApplicationContext()));
    }

    private void onClickClear() {
        etUrl.setText("");
        etStartTime.setText("");
        etEndTime.setText("");
    }

    private void initAlarmConfig()
    {
        alarmStart = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        intentStart = new Intent(getApplicationContext(), BootReceiver.class);

        alarmEnd = (AlarmManager) getSystemService(ALARM_SERVICE);
        intentEnd = new Intent(SettingsActivity.this, BootReceiver.class);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void onClickSave(View view) {
        if (TimeUrl.getDefaultUrl(getApplicationContext()).equals("")) {
            TimeUrl.setCurrentUrl(etDefaultUrl.getText().toString(), getApplicationContext());
        }
       // TimeUrl.setCurrentUrl(etDefaultUrl.getText().toString(),getApplicationContext());
        TimeUrl.setDefaultUrl(etDefaultUrl.getText().toString(), getApplicationContext());
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void onClickAdd(View view) {

        if (!etStartTime.getText().toString().isEmpty() && !etEndTime.getText().toString().isEmpty() && !etUrl.getText().toString().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("h:mm:ss a");

            try {
                Date sTime = sdf.parse(etStartTime.getText().toString());
                Date eTime = sdf.parse(etEndTime.getText().toString());
                if (isTimeAfter(sTime, eTime)) {
                    multipleAlarm();
                    timeUrl = new TimeUrl(etUrl.getText().toString(), etStartTime.getText().toString(), etEndTime.getText().toString(),mStartUniqueId,mEndUniqueID);
                    timeUrl.saveURLToSharedPreference(getApplicationContext());
                    loadListView();
                    onClickClear();
                } else {
                    Toast.makeText(this, "End time must be greater then Start time", Toast.LENGTH_SHORT).show();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public static long convertTimeToMillisecond(String time) {
        long timeinimilii;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:mm:ss a");
        Date date = null;
        try {
            date = simpleDateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar nw = Calendar.getInstance();
        nw.set(nw.get(Calendar.YEAR), nw.get(Calendar.MONTH), nw.get(Calendar.DAY_OF_MONTH), date.getHours(), date.getMinutes(),00);
        timeinimilii = nw.getTimeInMillis();
        return timeinimilii;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void multipleAlarm() {
        ArrayList<PendingIntent> pendingIntentArrayListStart = new ArrayList<>();
        mStartUniqueId = (int) System.currentTimeMillis();
        intentStart.setAction(START_TIME);
        pendingStart = PendingIntent.getBroadcast(getApplicationContext(),mStartUniqueId, intentStart,    PendingIntent.FLAG_UPDATE_CURRENT);
        alarmStart.setExact(AlarmManager.RTC_WAKEUP, convertTimeToMillisecond(etStartTime.getText().toString()), pendingStart);
        pendingIntentArrayListStart.add(pendingStart);

        ArrayList<PendingIntent> pendingIntentArrayListEnd = new ArrayList<>();
        mEndUniqueID = (int) System.currentTimeMillis();
        intentEnd.setAction(END_TIME);
        pendingEnd = PendingIntent.getBroadcast(SettingsActivity.this, mEndUniqueID, intentEnd,   PendingIntent.FLAG_UPDATE_CURRENT);
        alarmEnd.setExact(AlarmManager.RTC_WAKEUP, convertTimeToMillisecond(etEndTime.getText().toString()), pendingEnd);
        pendingIntentArrayListEnd.add(pendingEnd);

    }

    public boolean isTimeAfter(Date startTime, Date endTime) {
        return !endTime.before(startTime) && !endTime.equals(startTime);
    }


    private void loadListView() {
        if (TimeUrl.getSavedURLFromPreference(getApplicationContext()) != null) {
            timeURLAdapter = new TimeURLAdapter(getApplicationContext(), TimeUrl.getSavedURLFromPreference(getApplicationContext()), new TimeURLAdapter.TimeURLListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onDeleteClick(TimeUrl timeUrlDeleteDataList) {
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(  getApplicationContext(),   timeUrlDeleteDataList.startUniqueID, intentStart, PendingIntent.FLAG_UPDATE_CURRENT);
                    alarmStart.cancel(pendingIntent);

                    PendingIntent pendingIntent2 = PendingIntent.getBroadcast(  getApplicationContext(),   timeUrlDeleteDataList.endUniqueID, intentEnd, PendingIntent.FLAG_UPDATE_CURRENT);
                    alarmEnd.cancel(pendingIntent2);

                    TimeUrl.deleteFromSharedPreference(getApplicationContext(), timeUrlDeleteDataList.startUniqueID);
                    loadListView();
                }

                @Override
                public void onEditClick(TimeUrl timeUrlEditDataList) {
                    openEditDialog(timeUrlEditDataList);

                }
            });
            listView.setAdapter(timeURLAdapter);
        }
    }

    private void openEditDialog(TimeUrl timeUrlEditDatalist) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_setup_time_url, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setTitle("Edit URL");
        dialogBuilder.setCancelable(false);
        final EditText et_start_time = dialogView.findViewById(R.id.et_starttime);
        final EditText et_end_time = dialogView.findViewById(R.id.et_endtime);
        final EditText et_enter_url = dialogView.findViewById(R.id.et_enter_url);
        et_start_time.setText(timeUrlEditDatalist.start_time);
        et_end_time.setText(timeUrlEditDatalist.end_time);
        et_enter_url.setText(timeUrlEditDatalist.url);
        et_start_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSetTime(et_start_time);
            }
        });
        et_end_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSetTime(et_end_time);
            }
        });
        dialogBuilder.setPositiveButton("SAVE", new DialogInterface.OnClickListener() {
            @androidx.annotation.RequiresApi(api = Build.VERSION_CODES.O)
            public void onClick(DialogInterface dialog, int whichButton) {
                editRegisterMultipleAlarm(timeUrlEditDatalist,et_start_time.getText().toString(),et_end_time.getText().toString());
                timeUrl = new TimeUrl(timeUrlEditDatalist.url, timeUrlEditDatalist.start_time, timeUrlEditDatalist.end_time,timeUrlEditDatalist.startUniqueID,timeUrlEditDatalist.endUniqueID);
                timeUrl.editURLFromSharedPreference(getApplicationContext(), et_enter_url.getText().toString(), et_start_time.getText().toString(), et_end_time.getText().toString());
                loadListView();
                dialog.dismiss();
            }
        });
        dialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void editRegisterMultipleAlarm(TimeUrl timeUrllist,String startTime,String endTime) {
        ArrayList<PendingIntent> pendingIntentArrayListStart = new ArrayList<>();
        intentStart.setAction(START_TIME);
        pendingStart = PendingIntent.getBroadcast(getApplicationContext(), timeUrllist.startUniqueID, intentStart,    PendingIntent.FLAG_UPDATE_CURRENT);
        alarmStart.setExact(AlarmManager.RTC_WAKEUP, convertTimeToMillisecond(startTime), pendingStart);
        pendingIntentArrayListStart.add(pendingStart);

        ArrayList<PendingIntent> pendingIntentArrayListEnd = new ArrayList<>();
        intentEnd.setAction(END_TIME);
        pendingEnd = PendingIntent.getBroadcast(SettingsActivity.this, timeUrllist.endUniqueID, intentEnd,   PendingIntent.FLAG_UPDATE_CURRENT);
        alarmEnd.setExact(AlarmManager.RTC_WAKEUP, convertTimeToMillisecond(endTime), pendingEnd);
        pendingIntentArrayListEnd.add(pendingEnd);

    }

    private void onClickSetTime(final EditText editText) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(SettingsActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, selectedHour);
                calendar.set(Calendar.MINUTE, selectedMinute);
                calendar.set(Calendar.SECOND, 0);
                editText.setText(DateFormat.getTimeInstance(DateFormat.DEFAULT).format(calendar.getTime()));
            }
        }, hour, minute, false);//no 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void onClickCancel(View view) {
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {

    }
    @Override
    public void onClick(View view) {
        if (view == etStartTime) {
            onClickSetTime(etStartTime);
        } else if (view == etEndTime) {
            onClickSetTime(etEndTime);
        }
    }
}

